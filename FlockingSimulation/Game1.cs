﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FlockingBackend;

namespace FlockingSimulation
{   
    /// <summary>
    /// Represents a Game instance.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private World _world;
        private SparrowFlockSprite _sparrowSprite;
        private RavenSprite _ravenSprite;
        private Texture2D background;

        /// <summary>
        /// Constructor that initializes the World fields other default fields.
        /// </summary>
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _world = new World();
        }

        /// <summary>
        /// Sets the size of the window and add the Raven and Sparrow sprites as components of the game.
        /// </summary>
        protected override void Initialize()
        {
            // Set the window dimensions
            _graphics.PreferredBackBufferHeight = World.Height;
            _graphics.PreferredBackBufferWidth = World.Width;
            _graphics.ApplyChanges();

            // Create the sparrows and raven 
            _sparrowSprite = new SparrowFlockSprite(this, _world.Sparrows);
            _ravenSprite = new RavenSprite(this, _world.Raven);

            // Add to the game's components
            Components.Add(_sparrowSprite);
            Components.Add(_ravenSprite);

            base.Initialize();
        }

        /// <summary>
        /// Loads the background image for the game.
        /// Image attribution: https://wallpaperaccess.com/cartoon-aquarium
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>("aquarium");
            base.LoadContent();
        }

        /// <summary>
        /// Updates the game and calls the update method of the World object that fires all events in the Flock class.
        /// </summary>
        /// <param name="gameTime">The elapsed time since the last call to Update(GameTime).</param>
        protected override void Update(GameTime gameTime)
        {
            // Exits the window if the ESC button is pressed
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _world.Update();
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws he background image to the screen.
        /// </summary>
        /// <param name="gameTime">The elapsed time since the last call to Draw(GameTime).</param>
        protected override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            _spriteBatch.Draw(background, new Rectangle(0, 0, World.Width, World.Height), Color.White);
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
