using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using FlockingBackend;

namespace FlockingSimulation
{
    public class SparrowFlockSprite : DrawableGameComponent
    {
        private Game1 game;
        private List<Sparrow> sparrows;
        private Texture2D sparrowTexture;
        private SpriteBatch spriteBatch;
        // origin is used to specify the center of rotation for the sprite. It's half of the width/height of the sprite.
        private const int origin = 12;

        /// <summary>
        /// Constructor that calls the it's base constructor and initializes the game and 
        /// sparrow fields.
        /// </summary>
        /// <param name="game">Instance of Game1</param>
        /// <param name="raven">A list of sparrow instances</param>
        public SparrowFlockSprite(Game1 game, List<Sparrow> sparrows): base(game){
            this.game = game;
            this.sparrows = sparrows;
        }

        /// <summary>
        /// Loads the image needed to represent a sparrow.
        /// Image attribution: Clown Fish free icon by Freepik at Flaticon.com
        /// Link: https://www.flaticon.com/free-icon/clown-fish_875011?term=fish&related_id=874960&origin=search
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            sparrowTexture = game.Content.Load<Texture2D>("clown-fish");
            base.LoadContent();
        }

        /// <summary>
        /// Draws each sparrow at their position and rotation to the window.
        /// </summary>
        /// <param name="gameTime">Time elapsed since the last call to Draw(GameTime)</param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            foreach (Sparrow sparrow in sparrows)
            {
                spriteBatch.Draw(sparrowTexture, new Microsoft.Xna.Framework.Vector2(sparrow.Position.Vx, sparrow.Position.Vy), null, Color.White, sparrow.Rotation,
                                    new Microsoft.Xna.Framework.Vector2(origin, origin), 1, SpriteEffects.None, 0f);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

    }
}