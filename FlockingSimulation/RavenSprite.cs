using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FlockingBackend;

namespace FlockingSimulation
{
    /// <summary>
    /// Represents a Raven within the front end. 
    /// </summary>
    public class RavenSprite : DrawableGameComponent
    {
        private Game1 game;
        private Raven raven;
        private Texture2D ravenTexture;
        private SpriteBatch spriteBatch;
        // origin is used to specify the center of rotation for the sprite. It's half of the width/height of the sprite.
        private const int origin = 32;

        /// <summary>
        /// Constructor that calls the it's base constructor and initializes the game and 
        /// raven fields.
        /// </summary>
        /// <param name="game">Instance of Game1</param>
        /// <param name="raven">A raven instance</param>
        public RavenSprite(Game1 game, Raven raven): base(game){
            this.game = game;
            this.raven = raven;
        }

        /// <summary>
        /// Loads the needed image to represent a raven. To represent a raven, we decided to use a shark image instead.
        /// Image attribution: Shark free icon by Icongeek26 at Flaticon.com
        /// Link: https://www.flaticon.com/free-icon/shark_4054178?term=shark&related_id=4054563&origin=search
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ravenTexture = game.Content.Load<Texture2D>("shark");
            base.LoadContent();
        }

        /// <summary>
        /// Draws the raven to the window.
        /// </summary>
        /// <param name="gameTime">Time elapsed since the last call to Draw(GameTime)</param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(ravenTexture, new Microsoft.Xna.Framework.Vector2(raven.Position.Vx, raven.Position.Vy), null, Color.White, raven.Rotation,
                                    new Microsoft.Xna.Framework.Vector2(origin, origin), 1, SpriteEffects.None, 0f);
            spriteBatch.End();
            base.Draw(gameTime);
        }

    }
}