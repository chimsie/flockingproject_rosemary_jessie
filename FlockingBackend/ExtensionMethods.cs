using System.Collections.Generic;
using FlockingBackend;

namespace ExtensionMethods 
{
    public static class IEnumerableExtensions 
    {
        /// <summary>
        /// Extension method that calculates the sum of velocities of an enumerable of Sparrows.
        /// </summary>
        /// <param name="vectors">An IEnumerable of Sparrow objects</param>
        /// <returns>Sum of velocities of all sparrows</returns>
        public static Vector2 SumVelocities(this IEnumerable<Sparrow> sparrows) 
        {
            Vector2 sum = new Vector2();
            foreach (Sparrow sparrow in sparrows)
            {
                sum += sparrow.Velocity;
            }
            return sum;
        }

        /// <summary>
        /// Extension method that calculates the sum of positions of an enumerable of Sparrows.
        /// </summary>
        /// <param name="vectors">An IEnumerable of Sparrow objects</param>
        /// <returns>Sum of velocities of all sparrows</returns>
        public static Vector2 SumPositions(this IEnumerable<Sparrow> sparrows)
        {
            Vector2 sum = new Vector2();
            foreach (Sparrow sparrow in sparrows)
            {
                sum += sparrow.Position;
            }
            return sum;
        }
    }
    
}