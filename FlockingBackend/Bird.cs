using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    /// <summary>
    /// This abstract is used to represent a single bird.
    /// </summary>
    public abstract class Bird{

        /// <summary>
        /// hold the result of how much to move
        /// after applying the flocking rules and 
        // the raven fleeing rule.
        /// </summary>
        protected Vector2 amountToSteer;

        /// <summary>
        /// Used to calculate the amount by which the sparrow
        /// has to rotate to points towards the direction it is
        /// moving in.
        /// </summary>
        public float Rotation
        {
            get => (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
        }

        /// <summary>
        /// Used to represent the bird's position
        /// </summary>
        public Vector2 Position
        {
            get; protected set;
        }

        /// <summary>
        /// Used to represent the bird's velocity.
        /// </summary>
        public Vector2 Velocity
        {
            get; protected set;
        }

        /// <summary>
        /// will be determined by Bird's subclasses.
        /// </summary>
        public abstract void CalculateBehaviour(List<Sparrow> sparrows);

        /// <summary>
        /// Constructor that initializes the velocity vector to a
        /// random value between -4 and 4, the position vector to
        /// a random value between 0 and World.Height/World.Width
        /// and the amountToSteer vector to (0, 0).
        /// </summary>
        public Bird()
        {
            Random rand = new Random();
            this.Position = new Vector2(rand.Next(0, World.Width + 1), rand.Next(0, World.Height + 1));
            this.Velocity = new Vector2(rand.Next(-4, 5), rand.Next(-4, 5));
            this.amountToSteer = new Vector2(0, 0);
        }

        /// <summary>
        /// Overloaded constructor that will be used for testing and sets
        /// the Position and Velocity vectors using the input parameters to
        /// avoid randomization when unit testing.
        /// </summary>
        public Bird(float px, float py, float vx, float vy)
        {
            this.Position = new Vector2(px, py);
            this.Velocity = new Vector2(vx, vy);
            this.amountToSteer = new Vector2(0, 0);
        }

        /// <summary>
        /// Makes the bird appear on the opposite edge if it
        /// flies outside the edge of the screen.
        /// </summary>
        public void AppearOnOppositeSide()
        {
            if (this.Position.Vx > World.Width)
            {
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if(this.Position.Vx < 0)
            {
                 this.Position = new Vector2(World.Width, this.Position.Vy);
            }
            if (this.Position.Vy > World.Height)
            {
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if(this.Position.Vy < 0)
            {
                this.Position= new Vector2(this.Position.Vx, World.Height);
            }
        }

        /// <summary>
        /// event handler which applies the amountToSteer to move the sparrow.
        /// </summary>
        public virtual void Move()
        {
            this.Velocity += this.amountToSteer;
            this.Position += this.Velocity;
            AppearOnOppositeSide();
        }
    }
}
