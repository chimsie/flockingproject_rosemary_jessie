using System;

namespace FlockingBackend {
    /// <summary>
    /// Vector struct that contains x and y cordinates. 
    /// It contains overridden arithmetic operators and methods
    /// to calculate distance between two vectors.
    /// </summary>
    public struct Vector2 {
        /// <summary>
        /// Gets the x value of the vector
        /// </summary>
        public float Vx 
        {
            get;
        }
        /// <summary>
        /// Gets the y value of the vector
        /// </summary>
        public float Vy 
        {
            get;
        }
        /// <summary>
        /// Constructor that sets the x and y values of the vector
        /// </summary>
        /// <param name="x">X value</param>
        /// <param name="y">Y value</param>
        public Vector2(float x, float y) 
        {
            Vx = x;
            Vy = y;
        }

        /// <summary>
        /// Overrides the plus operator by adding the x and y values of 2 vectors
        /// and creating a new vector with these values.
        /// </summary>
        /// <param name="a">Vector 1</param>
        /// <param name="b">Vector 2</param>
        /// <returns></returns>
        public static Vector2 operator +(Vector2 a, Vector2 b) 
        {
            float newX = a.Vx + b.Vx;
            float newY = a.Vy + b.Vy;
            return new Vector2(newX, newY);
        }

        /// <summary>
        /// Overrides the plus operator by adding the x and y values of 2 vectors
        /// and creating a new vector with these values.
        /// </summary>
        /// <param name="a">Vector 1</param>
        /// <param name="b">Vector 2</param>
        /// <returns></returns>
        public static Vector2 operator -(Vector2 a, Vector2 b) 
        {
            float newX = a.Vx - b.Vx;
            float newY = a.Vy - b.Vy;
            return new Vector2(newX, newY);
        }

        /// <summary>
        /// Overrides the division operator by dividing the Vx and Yx Properties of a vector
        /// by a given float.
        /// </summary>
        /// <param name="a">Vector</param>
        /// <param name="num">Float divisor</param>
        /// <returns></returns>
        public static Vector2 operator /(Vector2 a, float num) 
        {
            return new Vector2(a.Vx / num, a.Vy / num);
        }

        /// <summary>
        /// Overrides the multiplication operator by multiplying the Vx and Vy values of two
        /// vectors. 
        /// </summary>
        /// <param name="a">Vector 1</param>
        /// <param name="b">Vector 2</param>
        /// <returns></returns>
        public static Vector2 operator *(Vector2 a, Vector2 b) 
        {
            float newX = a.Vx * b.Vx;
            float newY = a.Vy * b.Vy;
            return new Vector2(newX, newY);
        }

        /// <summary>
        /// Overrides the multiplication operator by multiplying the Vx and Vy values
        /// of a vector by a given float.
        /// </summary>
        /// <param name="vector">Vector</param>
        /// <param name="num">Float multiplier</param>
        /// <returns></returns>
        public static Vector2 operator *(Vector2 vector, float num) 
        {
            return new Vector2(vector.Vx * num, vector.Vy * num);
        }

        /// <summary>
        /// Returns the squared distance between two vectors according to 
        /// the following formula: dist(u, v) => ||u-v||^2 = (u1-v1)^2 + (u2-v2)^2
        /// </summary>
        /// <param name="a">Vector 1</param>
        /// <param name="b">Vector 2</param>
        public static float DistanceSquared(Vector2 a, Vector2 b)
        {
            float differenceX = a.Vx - b.Vx;
            float xSquared = differenceX * differenceX;
            float differenceY = a.Vy - b.Vy;
            float ySquared = differenceY * differenceY;
            // Return squared distance between both vectors
            float sum = xSquared + ySquared;
            return (float)Math.Sqrt(sum);  
        }

        /// <summary>
        /// Creates a new normalized vector given a vector.
        /// </summary>
        /// <param name="vector">Vector</param>
        /// <returns>New normalized vector</returns>
        public static Vector2 Normalize(Vector2 vector)
        {
            // We are using the following equation: ||v|| = sqrt(Vx*Vx + Vy*Vy)
            float vX = vector.Vx * vector.Vx;
            float vY = vector.Vy * vector.Vy;
            double magnitude = Math.Sqrt(vX + vY);
            // Divide the Vx and Vy values by the magnitude found above 
            float newX = (float)(vector.Vx / magnitude);
            float newY = (float)(vector.Vy / magnitude);
            // Return new vector with the new values above.
            return new Vector2(newX, newY);
        }
    }
}