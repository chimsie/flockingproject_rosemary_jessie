using System.Collections.Generic;

namespace FlockingBackend
{
    /// <summary>
    /// Delegate that will be used to raise the event to calculate the movement vector 
    /// for each sparrow and the raven.
    /// </summary>
    public delegate void CalculateMoveVector(List<Sparrow> sparrows);

    ///<summary>
    /// Delegate that will be used to raise the event required to move the sparrows and the raven.
    ///</summary>
    public delegate void MoveBird();

    ///<summary>
    /// Delegate that will be used to raise the event required to calculate the vector amount to steer
    /// to flee the chasing raven.
    public delegate void CalculateRavenAvoidance(Raven raven);
}