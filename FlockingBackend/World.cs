using System.Collections.Generic;

namespace FlockingBackend
{
    public class World
    {
        private Flock _flock;

        public List<Sparrow> Sparrows
        {
            get;
        }

        public Raven Raven
        {
            get;
        }

        /// <summary>
        /// number of sparrows
        /// </summary>
        public static int InitialCount
        {
            get; private set;
        }

        /// <summary>
        /// width of the canvas
        /// </summary>
        public static int Width
        {
            get; private set;
        }

        /// <summary>
        /// height of the canvas
        /// </summary>
        public static int Height
        {
            get; private set;
        }

        /// <summary>
        /// max speed of the birds
        /// </summary>
        public static int MaxSpeed
        {
            get; private set;
        }

        /// <summary>
        /// Radius used to determine if a bird is a neighbour
        /// </summary>
        public static int NeighbourRadius
        {
            get; private set;
        }

        /// <summary>
        /// Radius used to determine if a bird is too close
        /// </summary>
        public static int AvoidanceRadius
        {
            get; private set;
        }

        /// <summary>
        /// Static constructor that sets the static fields
        /// </summary>
        static World() {
            InitialCount = 150;
            Width = 1000;
            Height = 500;
            MaxSpeed = 4;
            NeighbourRadius = 100;
            AvoidanceRadius = 50;
        }

        /// <summary>
        /// Parameterless constructor that sets the instance fields and Properties of 
        /// the class.
        /// </summary>
        public World() 
        {
            _flock = new Flock();
            Raven = new Raven();
            // Subscribe raven to flock
            _flock.Subscribe(Raven.CalculateBehaviour, Raven.Move);

            this.Sparrows = new List<Sparrow>();
            for (int i = 0; i < InitialCount; i++)
            {
                Sparrow sparrow = new Sparrow();
                // Subscribe the sparrow to the Flock
                _flock.Subscribe(sparrow.CalculateBehaviour, sparrow.Move, sparrow.CalculateRavenAvoidance);
                this.Sparrows.Add(sparrow);
            }
        }

        public void Update()
        {
           _flock.RaiseMoveEvents(Sparrows, Raven);
        }
    }
}