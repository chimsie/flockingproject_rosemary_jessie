using System.Collections.Generic;
using System.Linq;

namespace FlockingBackend 
{
    /// <summary>
    /// This class is used to represent a single raven. 
    /// </summary>
    public class Raven : Bird
    {
        /// <summary>
        /// Constructor that calls the Bird class' parameterless constructor.
        /// </summary>
        public Raven(): base(){}

        /// <summary>
        /// Constructor that calls the Bird class' testing constructor.
        /// This constructor is only used for testing purposes.
        /// </summary>
        /// <param name="px">Position's x value</param>
        /// <param name="py">Position's y value</param>
        /// <param name="vx">Velocity's x value</param>
        /// <param name="vy">Velocity's y value</param>
        public Raven(float px, float py, float vx, float vy): base(px, py, vx, vy){}

        /// <summary>
        /// Adds the normalized velocity of the raven to its current Position. The velocity is normalized to it's max speed.
        /// </summary>
        public override void Move()
        {
            this.Velocity = Vector2.Normalize(this.Velocity + amountToSteer) * World.MaxSpeed;
            this.Position += this.Velocity;
            AppearOnOppositeSide();
        }
    
        /// <summary>
        /// Calculates the amountToSteer vector.
        /// </summary>
        /// <param name="sparrows">List of sparrow objects</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows)
        {
            // Adds the Vector2 from ChaseSparrow to amountToSteer
            amountToSteer = ChaseSparrow(sparrows);
        }

        /// <summary>
        /// This method adjusts the position of the raven to chase the nearest sparrow within its perception radius.
        /// </summary>
        /// <param name="sparrows">List of sparrow objects</param>
        /// <returns>Distance between this Raven and the closest sparrow</returns>
        private Vector2 ChaseSparrow(List<Sparrow> sparrows)
        {
            
            // Gets all the sparrows within the the Raven's avoidance radius 
            var neighbors = from sparrow in sparrows
                            where Vector2.DistanceSquared(this.Position, sparrow.Position) < World.AvoidanceRadius
                            select sparrow;

            // If neighbors is 0, that means the Raven has no neighbors so the default vector is returned.
            if (neighbors.Count() == 0)
            {
                return new Vector2();
            }

            // Gets the distance of the closest sparrow
            var closestDist = neighbors.Min(sp => Vector2.DistanceSquared(this.Position, sp.Position));
            
            // Gets the sparrow with the closest distance from the Raven. 
            // The Linq query returns an IEnumerable that contains 1 sparrow (the closest one), so we will get the element at the 0th position.
            var closestSparrow = (from sparrow in sparrows
                                 where Vector2.DistanceSquared(this.Position, sparrow.Position) == closestDist
                                 select sparrow).First();

            // Calculate and return the difference between the nearest sparrow’s position and the raven’s position.
            return closestSparrow.Position - this.Position;
        }
    }
}
