using System.Collections.Generic;
using System.Runtime.InteropServices; 

namespace FlockingBackend
{
    ///<summary>
    ///This class is the subscriber class that each bird subscribes to. 
    ///The class also raises the events to calculate movement vector and move the birds.
    ///</summary>
    public class Flock
    {
        public event CalculateMoveVector CalcMoveEvent;
        public event CalculateRavenAvoidance CalcRavenFleeEvent;
        public event MoveBird MoveEvent;

        /// <summary>
        /// Subscribes the parameters to their corresponding events.
        /// </summary>
        /// <param name="calcMoveVector">CalculateMoveVector delegate instance</param>
        /// <param name="moveBird">MoveBird delegate instance</param>
        /// <param name="calcRavenAvoid">optional CalculateRavenAvoidance delegate instance</param>
        public void Subscribe(CalculateMoveVector calcMoveVector, MoveBird moveBird, 
                [Optional]CalculateRavenAvoidance calcRavenAvoid)
        {
            CalcMoveEvent += calcMoveVector;
            MoveEvent += moveBird;
            // Since calcRavenAvoid is optional, we have to check when it's actually being passed through as a parameter. 
            // We have to check if the parameter is not null before it subscribes to CalcRavenFleeEvent.
            if (calcRavenAvoid != null)
            {
                CalcRavenFleeEvent += calcRavenAvoid;
            }  
        }

        ///<summary>
        ///This method raises the calculate and move events
        ///</summary>
        ///<param name="sparrows">List of Sparrow objects</param>
        ///<param name="raven">A Raven object</param>
        public void RaiseMoveEvents(List<Sparrow> sparrows, Raven raven)
        {
            CalcMoveEvent?.Invoke(sparrows);
            CalcRavenFleeEvent?.Invoke(raven);
            MoveEvent?.Invoke(); 
        }
    }
}