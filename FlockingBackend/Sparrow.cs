using System;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;

namespace FlockingBackend
{
    ///<summary>
    ///This class is used to represent a single sparrow. 
    ///This class is just a starting point. Complete the TODO sections
    ///</summary>
    public class Sparrow: Bird
    {
        /// <summary>
        /// Constructor that calls the base class (Bird) constructor.
        /// </summary>
        public Sparrow(): base(){}

        /// <summary>
        /// Constructor that calls the Bird class' testing constructor.
        /// This constructor is only used for testing purposes.
        /// </summary>
        /// <param name="px">Position's x value</param>
        /// <param name="py">Position's y value</param>
        /// <param name="vx">Velocity's x value</param>
        /// <param name="vy">Velocity's y value</param>
        public Sparrow(float px, float py, float vx, float vy): base(px, py, vx, vy){}

        /// <summary>
        /// Sets the amountToSteer as the sum of the vectors returned by Cohesion, Avoidance
        /// and alignment helper methods.
        /// </summary>
        public override void CalculateBehaviour(List<Sparrow> sparrows)
        {
            amountToSteer = Cohesion(sparrows) + Avoidance(sparrows) + Alignment(sparrows);
        }

        /// <summary>
        /// Adds the vector returned by FleeRaven helper method to amountToSteer to account for a Raven 
        /// that might be chasing the sparrow.
        /// </summary>
        /// <param name="raven">A Raven instance</param>
        public void CalculateRavenAvoidance(Raven raven)
        {
            this.amountToSteer += FleeRaven(raven);
        }

        /// <summary>
        /// Calculates how much a Sparrow should adjust its velocity to align
        /// itself with its neighboring Sparrows.
        /// </summary>
        /// <param name="sparrows">List of Sparrow instances</param>
        /// <returns>Average Velocity of neighboring sparrows</returns>
        private Vector2 Alignment(List<Sparrow> sparrows)
        {
            // Gets the neighboring sparrows and ensures that the current sparrow isn't included within the neighboring sparrows.
            var neighbors = from sparrow in sparrows
                            where Vector2.DistanceSquared(this.Position, sparrow.Position) < World.NeighbourRadius
                            && this != sparrow
                            select sparrow;

            // Count of neighboring sparrows
            int neighborCount = neighbors.Count();
           
            // If there are no neighbors at this point, the current sparrow can't align with another sparrow. Return the default vector.
            if (neighborCount == 0)
            {
                return new Vector2();
            }

            // gets the sum of the neighbors velocity
            Vector2 neighborsVelocity = neighbors.SumVelocities();

            // holds the average velocity
            Vector2 avgVelocity = neighborsVelocity / neighborCount;

            // holds the normalized velocity
            Vector2 normalizedAverageVelocity = Vector2.Normalize(avgVelocity) * World.MaxSpeed;

            // align sparrow's velocity to the neighbouring sparrows
            Vector2 result = Vector2.Normalize(normalizedAverageVelocity - this.Velocity);

            return result;
        }

        /// <summary>
        /// Calculates how much a Sparrow should adjust its position to match the average 
        /// position of its neighbouring sparrows.
        /// </summary>
        /// <param name="sparrows">List of Sparrow instances</param>
        /// <returns>Average position of neighboring sparrows</returns>
        private Vector2 Cohesion(List<Sparrow> sparrows)
        {
            // Gets the neighboring sparrows within the current sparrow's neighbor radius and ensures that the current 
            // sparrow isn't included within the neighboring sparrows.
            var neighbors = from sparrow in sparrows
                            where Vector2.DistanceSquared(this.Position, sparrow.Position) < World.NeighbourRadius
                            && this != sparrow
                            select sparrow;

            // Count of neighboring sparrows
            int neighborCount = neighbors.Count();
           
            // If there are no neighbors at this point, the current sparrow can't align with another sparrow. Return the default vector.
            if (neighborCount == 0)
            {
                return new Vector2();
            }

            // gets the sum of the neighbors positions
            Vector2 sumOfNeighborsPositions = neighbors.SumPositions();

            // holds the average position
            Vector2 avgPosition = sumOfNeighborsPositions / neighborCount;

            // move the sparrow to the calculated average position
            Vector2 displacementVector = avgPosition - this.Position;

            Vector2 displacementVectorNormalized = Vector2.Normalize(displacementVector) * World.MaxSpeed;

            // get new velocity after displacement
            Vector2 newVelocity = Vector2.Normalize(displacementVectorNormalized - this.Velocity);

            return newVelocity;
        }

        /// <summary>
        /// Returns the average difference velocity to adjust a sparrow's velocity. It is used to avoid 
        /// converging in nearby sparrows.
        /// </summary>
        /// <param name="sparrows">List of Sparrow instances</param>
        /// <returns>Average difference vector</returns>
        private Vector2 Avoidance(List<Sparrow> sparrows)
        {
            // Gets the sparrows within the current sparrow's avoidance radius and ensures that the current 
            // sparrow isn't included within the neighboring sparrows.
            var avoidSparrows = from sparrow in sparrows
                            where Vector2.DistanceSquared(this.Position, sparrow.Position) < World.AvoidanceRadius
                            && this != sparrow
                            select sparrow;

            // The sum of the difference between the current sparrow's position and the sparrows that are avoided
            // over the distance between both birds.
            Vector2 result = new Vector2();
            foreach (Sparrow sparrow in avoidSparrows)
            {
                float distance = Vector2.DistanceSquared(this.Position, sparrow.Position);
                result += (this.Position - sparrow.Position) / distance;
            }
            
            // get the count of sparrows to avoid
            int avoidSparrowsCount = avoidSparrows.Count();
           
            // If avoidSparrowsCount is 0, the current sparrow isn't colliding into another sparrow.
            if (avoidSparrowsCount == 0)
            {
                return new Vector2();
            }

            // average
            Vector2 avg = result / avoidSparrowsCount;

            // normalized average multiplied by MaxSpeed to get the magnitude
            Vector2 normalizedAvgMagnitude = Vector2.Normalize(avg) * World.MaxSpeed;

            // avoidance vector
            Vector2 finalVector = Vector2.Normalize(normalizedAvgMagnitude - this.Velocity);

            return finalVector;
        }

        /// <summary>
        /// Returns a Vector2 representing the amount to steer for a Sparrow to to flee Raven.
        /// </summary>
        /// <param name="raven">A raven instance</param>
        /// <returns>A vector representing the amount to steer away from a Raven</returns>
        private Vector2 FleeRaven(Raven raven)
        {
            // squared distance between current sparrow and raven
            float squaredDistance = Vector2.DistanceSquared(this.Position, raven.Position);

            if (squaredDistance < World.AvoidanceRadius)
            {
                // difference between Raven's and current sparrow's positions
                Vector2 posDifference = this.Position - raven.Position;

                Vector2 dividedDifferenceByDist = posDifference / squaredDistance;

                Vector2 result = Vector2.Normalize(dividedDifferenceByDist) * World.MaxSpeed;

                return result;
            }

            return new Vector2();
        }
    }
}
