using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;

namespace FlockingUnitTests
{
    /// <summary>
    /// This class tests the methods within the Raven class.
    /// For the purpose of this test, please make sure that you have set all the private 
    /// helper methods to public. You will also have to uncomment the TestMethods within this class
    /// in order to run the tests.
    /// </summary>
    [TestClass]
    public class RavenTest
    {
        /// <summary>
        /// Tests the helper method ChaseSparrow. It is expected that we receive the 
        /// distance of the closest sparrow to the raven.
        /// </summary>
        /*
        [TestMethod]
        public void ChaseSparrow_InputSparrows_ReturnShortestDistance()
        {
            // Arrange
            Raven raven = new Raven(40, 40, 0, 1);
            Sparrow sparrow1 = new Sparrow(50, 70, -1, 4);
            Sparrow sparrow2 = new Sparrow(50, 50, -3, 4);
            Sparrow sparrow3 = new Sparrow(200, 70, 0, 3);
            Sparrow[] sparrowArr = { sparrow1, sparrow2, sparrow3 };
            List<Sparrow> sparrows = new List<Sparrow>(sparrowArr);
            
            // The closest sparrow is sparrow2
            Vector2 shortestDistance = sparrow2.Position - raven.Position;

            // Act
            Vector2 distance = raven.ChaseSparrow(sparrows);

            // Assert 
            Assert.AreEqual(distance, shortestDistance);
        }

        /// <summary>
        /// Tests the helper method ChaseSparrow that expects to return the default Vector2 since there 
        /// is no Sparrow within the Raven's avoidance radius (no close sparrow).
        /// </summary>
        [TestMethod]
        public void ChaseSparrow_InputSparrows_ReturnDefaultVector()
        {
            // Arrange
            Raven raven = new Raven(40, 40, 0, 1);
            Sparrow sparrow1 = new Sparrow(100, 70, -1, 4);
            Sparrow sparrow2 = new Sparrow(800, 300, -3, 4);
            Sparrow sparrow3 = new Sparrow(200, 70, 0, 3);
            Sparrow[] sparrowArr = { sparrow1, sparrow2, sparrow3 };
            List<Sparrow> sparrows = new List<Sparrow>(sparrowArr);

            // Act
            Vector2 distance = raven.ChaseSparrow(sparrows);

            // Assert 
            Assert.AreEqual(distance, new Vector2());
        }
        */
    }
}