using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;

namespace FlockingUnitTests 
{
    [TestClass]
    public class Vector2Test
    {
        /// <summary>
        /// Tests the overridden Plus Operator that adds two given vectors into one.
        /// </summary>
        [TestMethod]
        public void PlusOperator_Input2Vectors_ReturnSumVector()
        {
            // Arrange 
            Vector2 vector1 = new Vector2(3, 4);
            Vector2 vector2 = new Vector2(5, 6);

            // Act
            Vector2 sumOfTwoVectors = vector1 + vector2;

            // Assert
            Assert.AreEqual(sumOfTwoVectors, new Vector2(8, 10));
        }

        /// <summary>
        /// Tests the overridden Minus Operator that substracts one vector from another.
        /// It is expected that we get one vector from the equation.
        /// </summary>
        [TestMethod]
        public void PlusOperator_Input2Vectors_ReturnDifferenceVector()
        {
            // Arrange
            Vector2 vector1 = new Vector2(3, 4);
            Vector2 vector2 = new Vector2(5, 6);

            // Act
            Vector2 differenceOfTwoVectors = vector1 - vector2;

            // Assert
            Assert.AreEqual(differenceOfTwoVectors, new Vector2(-2, -2));
        }

        /// <summary>
        /// Tests the overridden Division Operator that divides a vector by a given float.
        /// It is expected to return a single vector.
        /// </summary>
        [TestMethod]
        public void DivisionOperator_DivideByFloat_ReturnDividedVector()
        {
            // Arrange
            Vector2 vector = new Vector2(6,2);
            float divisor = 2;

            // Act
            Vector2 dividedVector = vector/divisor;

            // Assert
            Assert.AreEqual(dividedVector, new Vector2(3, 1));
        }

        /// <summary>
        /// Tests the overridden Multiplication Operator that multiplies two vectors together.
        /// It is expected to return a single vector.
        /// </summary>
        [TestMethod]
        public void MultiplicationOperator_Multiply2Vectors_Return1Vector()
        {
            // Arrange
            Vector2 vector1 = new Vector2(2, 2);
            Vector2 vector2 = new Vector2(4, 5);

            // Act  
            Vector2 multipliedVector = vector1 * vector2;

            // Assert
            Assert.AreEqual(multipliedVector, new Vector2(8, 10));
        }

        /// <summary>
        /// Tests the overridden Multiplication Operator that multiplies a vector by a given float.
        /// It is expected to return a single vector.
        /// </summary>
        [TestMethod]
        public void MultiplicationOperator_MultiplyVectorByFloat_Return1Vector()
        {
            // Arrange
            Vector2 vector1 = new Vector2(2, 5);
            float multiplier = 3;

            // Act
            Vector2 multipliedVector = vector1 * multiplier;

            // Assert
            Assert.AreEqual(multipliedVector, new Vector2(6, 15));
        }

        /// <summary>
        /// Tests the distance squared between two vectors. 
        /// </summary>
        [TestMethod]
        public void DistanceSquared_Input2Vectors_ReturnFloat()
        {
            // Arrange
            Vector2 vector1 = new Vector2(2, 6);
            Vector2 vector2 = new Vector2(1, 1);

            // Act
            float distance = Vector2.DistanceSquared(vector1, vector2);

            // Arrange
            Assert.AreEqual(distance, (float)Math.Sqrt(26));
        }

        /// <summary>
        /// Tests the Normalize method that normalizes a given vector.
        /// We expected to receive a normalized vector.
        /// </summary>
        [TestMethod]
        public void Normalize_Input1Vector_ReturnNormalizedVector()
        {
            // Arrange
            Vector2 vector = new Vector2(2, 6);
            // The following is used for the expected outcome. It is the exact same 
            // algorithm used to within the Normalize method.
            float vX = 2 * 2, vY = 6 * 6;
            double magnitude = Math.Sqrt(vX + vY);
            float newX = (float)(2 / magnitude);
            float newY = (float)(6 / magnitude);
            
            // Act
            Vector2 normalizedVector = Vector2.Normalize(vector);
            
            // Assert
            Assert.AreEqual(normalizedVector, new Vector2(newX, newY));
        }
    }
}
