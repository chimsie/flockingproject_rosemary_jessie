using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;
using System.Collections.Generic;

namespace FlockingUnitTests
{
    /// <summary>
    /// Class that will test the methods inside the
    /// Sparrow class. The helper methods should be
    /// set to public when you want to test them.
    /// Uncommment the methods to test.
    /// </summary>
    [TestClass]
    public class SparrowTest
    {

        /// <summary>
        /// Helper method for the test class to generate
        /// a list of Sparrows that will be within the
        /// neighbour radius.
        /// </summary>
        private List<Sparrow> getSparrowsWithNeighboursList()
        {
            List<Sparrow> sparrows = new List<Sparrow>();
            sparrows.Add(new Sparrow(4f, 4f, 5f, 5f));
            sparrows.Add(new Sparrow(6f, 6f, 7f, 7f));
            return sparrows;
        }

        /// <summary>
        /// Helper method for the test class to generate
        /// a list of Sparrows that will be outside the
        /// neighbour radius.
        /// </summary>
        private List<Sparrow> getSparrowsNoNeighboursList()
        {
            List<Sparrow> sparrows = new List<Sparrow>();
            sparrows.Add(new Sparrow(200f, 200f, 200f, 200f));
            sparrows.Add(new Sparrow(300f, 300f, 300f, 300f));
            sparrows.Add(new Sparrow(400f, 400f, 400f, 400f));
            return sparrows;
        }
        
        /*
        /// <summary>
        /// Verifies if Alignment() returns the expected
        /// alignment vector
        /// </summary>
        [TestMethod]
        public void Test_Alignment_Should_Be_Equal()
        {
            //arrange
            /// the list contains 2 sparrows that are neighbours with
            /// the current sparrow.
            List<Sparrow> sparrows = getSparrowsWithNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 alignment = sparrow.Alignment(sparrows);
            float newX = (float) (-2.171572875/Math.Sqrt(31.46089476));
            float newY = (float) (-5.171572875/Math.Sqrt(31.46089476));

            // assert
            Assert.AreEqual(alignment.Vx, newX, 0.01);
            Assert.AreEqual(alignment.Vy, newY, 0.01);
        }

        ///<summary>
        /// The Alignment method should return a vector (0, 0)
        /// because it has no neighbours.
        ///</summary>
        [TestMethod]
        public void Test_Alignment_No_Neighbours_Returns_Zero()
        {
            //arrange
            List<Sparrow> sparrows = getSparrowsNoNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 alignment = sparrow.Alignment(sparrows);

            // assert
            Assert.AreEqual(alignment.Vx, 0);
            Assert.AreEqual(alignment.Vy, 0);
        }

        /// <summary>
        /// Verifies if Cohesion() returns the expected
        /// cohesion vector
        /// </summary>
        [TestMethod]
        public void Test_Cohesion_Should_Be_Equal()
        {
            //arrange
            List<Sparrow> sparrows = getSparrowsWithNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 cohesion = sparrow.Cohesion(sparrows);
            float newX = (float) (-2.171572875/Math.Sqrt(31.46089476));
            float newY = (float) (-5.171572875/Math.Sqrt(31.46089476));

            // assert
            Assert.AreEqual(cohesion.Vx, newX, 0.01);
            Assert.AreEqual(cohesion.Vy, newY, 0.01);
        }

        ///<summary>
        /// The Cohesion method should return a vector (0, 0)
        /// because it has no neighbours.
        ///</summary>
        [TestMethod]
        public void Test_Cohesion_No_Neighbours_Returns_Zero()
        {
            //arrange
            List<Sparrow> sparrows = getSparrowsNoNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 cohesion = sparrow.Cohesion(sparrows);

            // assert
            Assert.AreEqual(cohesion.Vx, 0);
            Assert.AreEqual(cohesion.Vy, 0);
        }

        /// <summary>
        /// Verifies if Avoidance() returns the expected
        /// avoidance vector
        /// </summary>
        [TestMethod]
        public void Test_Avoidance_Should_Be_Equal()
        {
            //arrange
            List<Sparrow> sparrows = getSparrowsWithNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 avoidance = sparrow.Avoidance(sparrows);
            float newX = -0.5858789f;
            float newY = -0.81039864f;

            // assert
            Assert.AreEqual(avoidance.Vx, newX, 0.01);
            Assert.AreEqual(avoidance.Vy, newY, 0.01);
        }

        ///<summary>
        /// The Avoidance method should return a vector (0, 0)
        /// because it has no neighbours.
        [TestMethod]
        public void Test_Avoidance_No_Neighbours_Returns_Zero()
        {
            //arrange
            List<Sparrow> sparrows = getSparrowsNoNeighboursList();
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);

            // act
            Vector2 avoidance = sparrow.Avoidance(sparrows);

            // assert
            Assert.AreEqual(avoidance.Vx, 0);
            Assert.AreEqual(avoidance.Vy, 0);
        }

        /// <summary>
        /// Verifies if the FleeRaven method returns the expected
        /// vector.
        ///</summary>
        [TestMethod]
        public void Test_FleeRaven_Should_Be_Equal()
        {
            //arrange
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);
            Raven raven = new Raven(4f, 3f, 2f, 1f);

            // act
            Vector2 flee = sparrow.FleeRaven(raven);
            float newX = -3.577708764f;
            float newY = -1.788854382f;

            // assert
            Assert.AreEqual(flee.Vx, newX, 0.01);
            Assert.AreEqual(flee.Vy, newY, 0.01);
        }

        /// <summary>
        /// Verifies if FleeRaven returns a vector (0, 0)
        /// when the sparrow is out of the raven's avoidance
        /// radius.
        /// </summary>
        [TestMethod]
        public void Test_FleeRaven_No_Raven_Aside()
        {
            //arrange
            Sparrow sparrow = new Sparrow(2f, 2f, 5f, 8f);
            Raven raven = new Raven(60f, 60f, 60f, 60f);

            // act
            Vector2 flee = sparrow.FleeRaven(raven);

            // assert
            Assert.AreEqual(flee.Vx, 0, 0.01);
            Assert.AreEqual(flee.Vy, 0, 0.01);
        }
        */
    }
}