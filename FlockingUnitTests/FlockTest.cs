using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;

namespace FlockingUnitTests
{
    /// <summary>
    /// Tests the Flock class's methods.
    /// </summary>
    [TestClass]
    public class FlockTest
    {
        private Flock _flock;
        private Raven _raven;
        private Sparrow _sparrow1, _sparrow2, _sparrow3;
        private List<Sparrow> _sparrows;

        /// <summary>
        /// Initializes all private fields. To be run before each Test method. 
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _flock = new Flock();
            _raven = new Raven(40, 40, 0, 1);
            Sparrow[] sparrows = new Sparrow[] { 
                _sparrow1 = new Sparrow(50, 70, -1, 4),
                _sparrow2 = new Sparrow(50, 50, -3, 4), 
                _sparrow3 = new Sparrow(200, 70, 0, 3) 
            };
            _sparrows = new List<Sparrow>(sparrows);
        }

        /// <summary>
        /// To be used after each test is run.
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            _flock = null;
            _raven = null;
            _sparrow1 = null;
            _sparrow2 = null; 
            _sparrow3 = null;
            _sparrows.Clear();
        }

        /// <summary>
        /// Tests to verify that the Flock's subscribe method works correctly when a Raven subscribes to it.
        /// </summary>
        [TestMethod]
        public void Subscribe_Raven_PositionChanged()
        {
            // Arrange
            // This will be used to calculate the new Position by adding the raven's new velocity to it.
            Vector2 oldPosition = _raven.Position;

            // Act
            _flock.Subscribe(_raven.CalculateBehaviour, _raven.Move);
            _flock.RaiseMoveEvents(_sparrows, _raven);

            // Assert
            Assert.AreEqual(oldPosition + _raven.Velocity, _raven.Position);
        }

        /// <summary>
        /// Tests to verify that the Flock's subscribe method works correctly when a Sparrow subscribes to it.
        /// </summary>
        [TestMethod]
        public void Subscribe_Sparrow_PositionChanged()
        {
            // Arrange
            // This will be used to calculate the new Position by adding the sparrow's new velocity to it.
            Vector2 sp1OldPosition = _sparrow1.Position;
            Vector2 sp2OldPosition = _sparrow2.Position;
            Vector2 sp3OldPosition = _sparrow3.Position;

            // Act
            foreach (Sparrow sparrow in _sparrows)
            {
                _flock.Subscribe(sparrow.CalculateBehaviour, sparrow.Move, sparrow.CalculateRavenAvoidance);
            }
            _flock.RaiseMoveEvents(_sparrows, _raven);

            // Assert
            Assert.AreEqual(sp1OldPosition + _sparrow1.Velocity, _sparrow1.Position);
            Assert.AreEqual(sp2OldPosition + _sparrow2.Velocity, _sparrow2.Position);
            Assert.AreEqual(sp3OldPosition + _sparrow3.Velocity, _sparrow3.Position);
        }
    }
}